# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name :NESS 論壇
* Key functions (add/delete)
    1. 登入/登出 ( facebook | google )
    2. 注冊
    3. 觀看/新增貼文
    4. 留言
    5. 個人資訊管理
    
* Other functions (add/delete)
    1. 找回密碼
    2. 修改密切
    3. 獨立名字
    4. 修改名字
    5. 上傳頭像
    6. 同步帳號資訊
    7. 分頁


## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://ness-forum.firebaseapp.com/index.html

# Components Description : 
1. Membership Mechanism : 透過email注冊登入，然後系統會把帳號資訊生成在database裏。
2. Firebase Page : https://ness-forum.firebaseapp.com/index.html
3. Database : 可以讀取和新增貼文，讀取和修改帳號資訊。
4. RWD : 在不同裝置上，沒有overflow。
5. Topic Key Functions : 用戶帳號頁面、不同討論的區域、觀看/新增金貼文、留言。
6. Third-party accounts : Google, facebook.
7. Chrome notification : 當你的貼文有新的回應時，會有一個Chrome notification提醒。
8. CSS animation : 選擇動畫、Loading 動畫。

# Other Functions Description(1~10%) : 
1. 找回密碼 : 寄一封可以修改一密碼的email。
2. 修改密碼 : 登入後，可以在你的user page上更新你的密碼。
3. 獨立名字 : 每個user都會有獨立的名字。
4. 修改名字 : 在user page上可以修改名字，也會檢查名字是否存在。
5. 上傳頭像 : 每個user都可以上傳自己的頭像。
6. 同步帳號資訊 : 更新名字或頭像後，貼文裹的資訊也會隨著更新。
7. 分頁 : 貼文有做分頁（按照發起貼文的時間排序）


## Security Report (Optional)
1. 沒有登入不能修改database。
2. 沒有登入不能修改storage。
3. 不會加入 html code。
4.  加密和最小化 javascript 檔，令到用戶不易進行修改的行為。(Firebase Page)
