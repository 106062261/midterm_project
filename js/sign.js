function emailLogIn(){
    var email = $("#email").val();
    var pw = $("#password").val();
    loading(true);
    firebase.auth().signInWithEmailAndPassword(email, pw).then(()=>{
        var user = firebase.auth().currentUser;
        var profileRef = firebase.database().ref("user/profile").orderByChild("email").equalTo(user.email);

        profileRef.once("value").then(snp=>{
            var name;
            var photo;
            $.each(snp.val(),(i,item)=>{
                name = item.name;
                photo = item.photo;
            })
            var photoStr = "img/"+photo;
            var storageRef = firebase.storage().ref(photoStr);

            storageRef.getDownloadURL().then(url=>{
                user.updateProfile({displayName:name,photoURL:url}).then(()=>{
                    loading(false);
                    window.location.href = "index.html";
                }); 
            }).catch(()=>{
                var url = "https://firebasestorage.googleapis.com/v0/b/ness-forum.appspot.com/o/img%2Fdefine_icon.png?alt=media&token=cfc482a5-8c88-4c0d-a5c0-2bcde0886be7";
                user.updateProfile({displayName:name,photoURL:url}).then(()=>{
                    loading(false);
                    window.location.href = "index.html";
                });
            })
        })
    }).catch(error=>{
        loading(false);
        alert(error.message);
    })
}

function facebookLogIn(){
    var provider = new firebase.auth.FacebookAuthProvider();
    loading(true);

        firebase.auth().signInWithPopup(provider).then((result)=>{
            var profileRef = firebase.database().ref("user/profile").orderByChild("email").equalTo(result.user.email);
            profileRef.once("value").then(snp=>{
                if(!snp.val()){
                    var userCounterRef = firebase.database().ref("user/userCounter");
                    userCounterRef.once("value").then(snp_=>{
                        userCounterRef.set(parseInt(snp_.val()) + 1);

                        firebase.database().ref("user/profile").push({
                            name : "noob" + (parseInt(snp_.val()) + 1),
                            email : result.user.email,
                            photo : "noob" + (parseInt(snp_.val()) + 1) + ".png"
                        },()=>{
                            var profileRef = firebase.database().ref("user/profile").orderByChild("email").equalTo(result.user.email);
                        
                            profileRef.once("value").then(snp=>{
                                var name, photo;
                    
                                $.each(snp.val(),(i,item)=>{
                                    name = item.name;
                                    photo = item.photo;
                                })
                                var photoStr = "img/"+photo;
                                var storageRef = firebase.storage().ref(photoStr);

                                storageRef.getDownloadURL().then(url=>{
                                    result.user.updateProfile({displayName:name,photoURL:url}).then(()=>{
                                        loading(false);
                                        window.location.href = "index.html";
                                    });
                                }).catch(()=>{
                                    var url = "https://firebasestorage.googleapis.com/v0/b/ness-forum.appspot.com/o/img%2Fdefine_icon.png?alt=media&token=cfc482a5-8c88-4c0d-a5c0-2bcde0886be7";
                                    result.user.updateProfile({displayName:name,photoURL:url}).then(()=>{
                                        loading(false);
                                        window.location.href = "index.html";
                                    });
                                })
                            })
                        })
                    }).catch(e=>{
                        loading(false);
                    })

                }else{
                    var name, photo;

                    $.each(snp.val(),(i,item)=>{
                        name = item.name;
                        photo = item.photo;
                    })

                    var photoStr = "img/"+photo;

                    var storageRef = firebase.storage().ref(photoStr);

                    storageRef.getDownloadURL().then(url=>{
                        result.user.updateProfile({displayName:name,photoURL:url}).then(()=>{
                            loading(false);
                            window.location.href = "index.html";
                        });
                    }).catch(()=>{
                        var url = "https://firebasestorage.googleapis.com/v0/b/ness-forum.appspot.com/o/img%2Fdefine_icon.png?alt=media&token=cfc482a5-8c88-4c0d-a5c0-2bcde0886be7";
                        result.user.updateProfile({displayName:name,photoURL:url}).then(()=>{
                            loading(false);
                            window.location.href = "index.html";
                        });
                    })
                } 
            }).catch(e=>{
                loading(false);
            })
        }).catch((error)=>{
            loading(false);
            alert(error.message);
        })
}

function googleLogIn(){
    var provider = new firebase.auth.GoogleAuthProvider();
    loading(true);
    provider.addScope('https://www.googleapis.com/auth/contacts.readonly');

    firebase.auth().signInWithPopup(provider).then(result => {
        var profileRef = firebase.database().ref("user/profile").orderByChild("email").equalTo(result.user.email);
        profileRef.once("value").then(snp=>{
            if(!snp.val()){
                var userCounterRef = firebase.database().ref("user/userCounter");
                userCounterRef.once("value").then(snp_=>{
                    userCounterRef.set(parseInt(snp_.val()) + 1);

                    firebase.database().ref("user/profile").push({
                        name: "noob" + (parseInt(snp_.val()) + 1),
                        email: result.user.email,
                        photo: "noob" + (parseInt(snp_.val()) + 1) + ".png"
                    },()=>{
                        var profileRef = firebase.database().ref("user/profile").orderByChild("email").equalTo(result.user.email);
                        
                        profileRef.once("value").then(snp=>{
                            var name, photo;
                
                            $.each(snp.val(),(i,item)=>{
                                name = item.name;
                                photo = item.photo;
                            })
                            var photoStr = "img/"+photo;
                            var storageRef = firebase.storage().ref(photoStr);

                            storageRef.getDownloadURL().then(url=>{
                                result.user.updateProfile({displayName:name,photoURL:url}).then(()=>{
                                    loading(false);
                                    window.location.href = "index.html";
                                });
                            }).catch(()=>{
                                var url = "https://firebasestorage.googleapis.com/v0/b/ness-forum.appspot.com/o/img%2Fdefine_icon.png?alt=media&token=cfc482a5-8c88-4c0d-a5c0-2bcde0886be7";
                                result.user.updateProfile({displayName:name,photoURL:url}).then(()=>{
                                    loading(false);
                                    window.location.href = "index.html";
                                });
                            })
                        })
                    })


                }).catch(e=>{
                    loading(false);
                })

            }else{
                var name, photo;
                
                $.each(snp.val(),(i,item)=>{
                    name = item.name;
                    photo = item.photo;
                })
                var photoStr = "img/"+photo;
                var storageRef = firebase.storage().ref(photoStr);

                storageRef.getDownloadURL().then(url=>{
                    result.user.updateProfile({displayName:name,photoURL:url}).then(()=>{
                        loading(false);
                        window.location.href = "index.html";
                    });
                }).catch(()=>{
                    var url = "https://firebasestorage.googleapis.com/v0/b/ness-forum.appspot.com/o/img%2Fdefine_icon.png?alt=media&token=cfc482a5-8c88-4c0d-a5c0-2bcde0886be7";
                    result.user.updateProfile({displayName:name,photoURL:url}).then(()=>{
                        loading(false);
                        window.location.href = "index.html";
                    });
                })
            } 

        }).catch(e=>{
            loading(false);
        })
    }).catch(error => {
        loading(false);
        alert(error.message);
    })
}

function loading(state){
    if(state == true){
        $("#loading").css("display","block");
    }else{
        $("#loading").css("display","none");
    }
}