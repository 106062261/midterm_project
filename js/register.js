
function checkPw(){
    var pw = $("#password").val();
    var pw_ = $("#password_").val();

    if(pw == pw_ & pw.length > 5){
        $("#pw-check").css("background-color","rgb(23,223,23)");
        return true;
    }else{
        $("#pw-check").css("background-color","red");
        return false;
    }
}

function register(){
    if(checkPw()){
        var email = $("#email").val();
        var pw = $("#password").val();
        loading(true);
        firebase.auth().createUserWithEmailAndPassword(email, pw).then(()=>{
            var userCounterRef = firebase.database().ref("user/userCounter");
            var profileRef = firebase.database().ref("user/profile");

            userCounterRef.once("value").then(snp=>{
                var profile = {
                    name : "noob" + (parseInt(snp.val()) + 1),
                    email : email,
                    photo : "noob" + (parseInt(snp.val()) + 1) + ".png"
                }

                userCounterRef.set(parseInt(snp.val()) + 1)
                profileRef.push(profile);

                alert("註冊成功");
                window.location.href = "signPage.html";
            }).catch(e=>{
                loading(false);
            })
        }).catch(e=>{
            alert(e.message);
            loading(false);
        })
    }else{
        alert("密碼錯誤");
    }
}

function loading(state){
    if(state == true){
        $("#loading").css("display","block");
    }else{
        $("#loading").css("display","none");
    }
}